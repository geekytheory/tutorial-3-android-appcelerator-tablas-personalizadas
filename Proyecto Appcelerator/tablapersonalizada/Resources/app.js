/**
 * 
 * author: Mario
 * Tutorial 3 Appcelerator: tablas personalizadas
 * 
 */
var ventana=Ti.UI.createWindow({
	backgroundColor: '#FFF',
	Title: 'Países'
});
paises=[
			{title: 'España', habitantes: '47.000.000', capital: 'Madrid', leftImage: 'espana.png'},
			{title: 'México', habitantes: '115.000.000', capital: 'México, D.F.' ,leftImage: 'mexico.png'},
			{title: 'Colombia', habitantes: '46.500.000', capital: 'Bogota',leftImage: 'colombia.png'},
			{title: 'Argentina', habitantes: '40.000.000', capital: 'Buenos Aires',leftImage: 'argentina.png'},
			{title: 'EEUU', habitantes: '309.000.000', capital: 'Washington D.C.',leftImage: 'eeuu.png'},
			{title: 'Chile', habitantes: '17.000.000', capital: 'Santiago',leftImage: 'chile.png'},
			{title: 'Brasil', habitantes: '194.000.000', capital: 'Brasilia',leftImage:'brasil.png'},
			{title: 'Ecuador', habitantes: '15.000.000', capital: 'Quito',leftImage: 'ecuador.png'},
			{title: 'Venezuela', habitantes: '27.000.000', capital: 'Caracas',leftImage: 'venezuela.png'},
			{title: 'Italia', habitantes: '61.000.000', capital: 'Roma',leftImage: 'italia.png'}
			];
tabla=Ti.UI.createTableView({
	data: paises,
	backgroundColor: 'white'
});
tabla.addEventListener('click', function(e){
	alert("Pais: "+e.rowData.title+"\nCapital: "+e.rowData.capital+"\nHabitantes: "+e.rowData.habitantes);
});
ventana.add(tabla);
ventana.open();
